require './lib/thumb_it/version'

Gem::Specification.new do |s|
  s.name          = 'thumb-it'
  s.version       = ThumbIt::VERSION
  s.authors       = ['Orgânica Digital', 'Alessandro Tegner']
  s.email         = ['alessandro@organicadigital.com']
  s.summary       = 'Thumb It Integration gem'
  s.description   = s.summary
  s.homepage      = 'http://organicadigital.com'
  s.license       = 'MIT'

  s.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec)/}) }
  s.bindir        = 'exe'
  s.executables   = s.files.grep(%r{^exe/}) { |f| File.basename(f) }
  s.require_paths = ['lib']

  # s.add_dependency 'rest-client', '~> 1.8.0'
  s.add_dependency 'httmultiparty', '~> 0.3.16'
  s.add_dependency 'token-die', '0.0.3'
  s.add_dependency 'multi_json', '~> 1.11.2'
  s.add_development_dependency 'bundler', '>= 1.8'
  s.add_development_dependency 'rake', '>= 10.0'
  s.add_development_dependency 'rspec', '>= 3.3.0'
  s.add_development_dependency 'fakeweb', '>= 1.3.0'
end