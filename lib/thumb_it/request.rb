require 'httmultiparty'
require 'multi_json'

module ThumbIt
  module Request
    extend self

    def self.timeout
      @timeout ||= 120
    end

    def self.timeout=(value)
      @timeout = value
    end

    def post(url, args = {})
      response = HTTMultiParty.post(url, query: args, timeout: self.timeout)

      return decode_json(response.body) if response.success?

      case response.code
      when 422
        errors = decode_json(response.body)
        raise UnsavedResourceError.new errors, "Unprocessable Entity"
      when 401
        raise UnauthorizedError
      when 404
        raise NotFoundError
      else
        raise UnspectedResponseError, "Unspected response => #{response.code}"
      end
    end

    private
      def decode_json(content)
        MultiJson.decode(content, symbolize_keys: true)
      end

  end
end