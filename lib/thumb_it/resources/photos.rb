module ThumbIt
  module Resources
    class Photos < Base
      def create_url
        url_for("/photos.json")
      end

      def create(args = {})
        ThumbIt::Request.post create_url, {photo: args}
      end
    end
  end
end