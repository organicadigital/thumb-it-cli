module ThumbIt
  module Resources
    class Base < Struct.new(:client)
      def url_for(path)
        client.url_helper.url_for(path)
      end
    end
  end
end