require 'token-die'
require 'uri'

module ThumbIt
  class URLHelper
    attr_reader :app_name, :secret_token, :base_uri, :sso
    def initialize(app_name, secret_token, base_uri)
      @app_name = app_name
      @secret_token = secret_token
      @sso = TokenDie.new secret_token
      @base_uri = base_uri
    end

    def access_token
      sso.generate
    end

    def url_for(path)
      query = to_query({access_token: self.access_token})

      File.join(self.base_uri, "app-#{app_name}", path) + "?" + query
    end

    def to_query(args = {})
      URI.encode_www_form(args)
    end
  end
end
