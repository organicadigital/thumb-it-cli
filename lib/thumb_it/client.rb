module ThumbIt
  class Client < Struct.new(:app_name, :secret_token, :base_uri)
    def url_helper
      @url_helper ||= URLHelper.new(app_name, secret_token, base_uri)
    end

    def photos
      @photos ||= Resources::Photos.new self
    end
  end
end
