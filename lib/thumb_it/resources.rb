module ThumbIt
  module Resources
    autoload :Base, 'thumb_it/resources/base'
    autoload :Photos, 'thumb_it/resources/photos'
  end
end
