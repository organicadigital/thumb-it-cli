module ThumbIt
  autoload :Client, 'thumb_it/client'
  autoload :Request, 'thumb_it/request'
  autoload :URLHelper, 'thumb_it/url_helper'
  autoload :Version, 'thumb_it/version'
  autoload :Resources, 'thumb_it/resources'

  class UnauthorizedError < StandardError; end
  class NotFoundError < StandardError; end
  class UnspectedResponseError < StandardError; end
  class UnsavedResourceError < StandardError
    attr_accessor :resource_errors

    def initialize(resource_errors, error)
      super(error)

      @resource_errors = resource_errors
    end
  end

  class << self
    def base_uri
      ENV.fetch('BASE_URI', 'http://www.thumb321.com.br')
    end
  end
end
