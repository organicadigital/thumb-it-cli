require 'spec_helper'

describe ThumbIt::URLHelper do
  subject { ThumbIt::URLHelper.new "foo", "abc-xyz" }

  describe '.new' do
    it 'creates a new SSO Instance' do
      expect(TokenDie).to receive(:new).with('abc-xyz')

      ThumbIt::URLHelper.new 'foo', 'abc-xyz'
    end
  end

  describe "#access_token" do
    it 'delegates to SSO instance' do
      expect(subject.sso).to receive(:generate)

      subject.access_token
    end
  end

  describe "#url_for" do
    it "generates a url with access token" do
      base_uri = ThumbIt.base_uri

      allow(subject).to receive_messages(access_token: "efg-yx")

      expect(subject.url_for("/foo/bar")).to eql("#{base_uri}/app-foo/foo/bar?access_token=efg-yx")
    end
  end
end