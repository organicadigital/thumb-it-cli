require 'spec_helper'

describe ThumbIt::Resources::Photos do
  let(:client) { ThumbIt::Client.new('foo', 'abc-xyz') }
  subject { ThumbIt::Resources::Photos.new client }

  before do
    args = {status: 201, body: '{"created": true}'}
    uri = %r[#{ThumbIt.base_uri}/app-foo/photos\.json\?access_token=.*]

    FakeWeb.register_uri :post, uri, args
  end

  describe "#create" do
    it 'creates a photo' do
      response = subject.create foo: 'bar'
      expect(response).to be_eql({created: true})
    end
  end
end