require 'spec_helper'

describe ThumbIt::Request do
  describe '.post' do
    let(:file_upload) { File.new('spec/fixtures/file.txt') }

    before do
      FakeWeb.register_uri :post, %r|http://foo/bar|, status: 201,
        body: '{"foo": "bar"}', content_type: 'application/json'
    end

    it 'submit request by Post' do
      subject.post 'http://foo/bar?foo=bar', {foo: 'bar', hello: {world: 'foo'}}
      request = FakeWeb.last_request

      expect(request.method).to be_eql('POST')
    end

    it 'returns a response object' do
      expect(subject.post('http://foo/bar')).to be_eql({foo: 'bar'})
    end

    it 'detects 422 status' do
      FakeWeb.register_uri :post, "http://foo/bar", status: 422,
        body: '["Some error"]', content_type: 'application/json'

      expect {
        subject.post("http://foo/bar")
      }.to raise_error(ThumbIt::UnsavedResourceError)
    end

    it 'detects 404 status' do
      FakeWeb.register_uri :post, "http://foo/bar", status: 404

      expect {
        subject.post("http://foo/bar")
      }.to raise_error(ThumbIt::NotFoundError)
    end

    it 'detects 401 status' do
      FakeWeb.register_uri :post, "http://foo/bar", status: 401

      expect {
        subject.post("http://foo/bar")
      }.to raise_error(ThumbIt::UnauthorizedError)
    end
  end
end