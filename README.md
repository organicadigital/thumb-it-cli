# ThumbIt Client

Client library to handle on ThumbIt.

## Usage

```ruby
app_name = 'my-app' #=> App name
secret = 'my-secret' #=> Secret token

thumbit = ThumbIt::Client.new(app_name, secret)

#=> Creates a photo from file
file = File.new('my-avatar.png')
photo = thumbit.photos.create(image: file)
photo[:url] #=> http://thumbit.site321.com.br/img-c2b840cd8ada3245

#=> Creates a photo from URL
endpoint = 'http://img1.wikia.nocookie.net/__cb20150420161252/gameofthrones/images/c/c2/Drogon_5x02.jpg'
photo = thumbit.photos.create(load_from_url: endpoint)
photo[:url] #=> http://thumbit.site321.com.br/img-c2b840cd8ada3245

## Errors
```